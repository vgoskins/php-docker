FROM php:7.1-fpm

RUN apt-get update -qy && \
    apt-get install -qy nano \
        nginx \
        apt-utils \
        iputils-ping \
        libicu-dev \
        libz-dev \
        libpng-dev \
        libc-client-dev \
        libkrb5-dev \
        libmcrypt-dev \
        libxml2-dev \
        libtidy-dev \
        libodbc1 \
        locate \
        iproute2 \
        curl \
        wget \
        pdftk \
        unzip \
        pdftk \
        imagemagick \
        gettext \
        libevent-dev && \
    rm -r /var/lib/apt/lists/*

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl

RUN docker-php-ext-install \
        imap \
        pdo \
        pdo_mysql \
        mysqli \
        bcmath \
        dba \
        gd \
        intl \
        mcrypt \
        opcache \
        soap \
        tidy \
        zip \
        xml \
        xmlrpc

RUN pecl install channel://pecl.php.net/xdebug-2.6.0 && \
    wget -O /tmp/composer https://getcomposer.org/composer.phar && \
    mv /tmp/composer /usr/local/bin/composer

RUN pecl install apcu ev && docker-php-ext-enable apcu ev pdo_mysql
